#!/bin/bash
set -x

# libpsm no longer support "shm". User has to run libpsm2 test over HFI hardware.
# Unfortunately, osci gating system does not support submit test to specific machine
# in beaker system. That is why this test is just a placeholder.
